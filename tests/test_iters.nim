import iterutils

proc ccountdown(i: int): iterator: int =
  return iterator(): int =
    var x = i
    while x > 0:
      yield x
      dec(x)

proc ccountup(i: int): iterator: int = 
  return iterator(): int =
    var x = i
    while x > 0:
      yield x
      x.inc

when isMainModule:
  var counter = 0
  for (x, g) in zip(ccountup(10), ccountdown(10)):
    assert x == 10 + counter
    assert g == 10 - counter
    counter.inc
