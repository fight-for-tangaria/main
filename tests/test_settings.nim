import unittest, options
import os

import utilities/readers 

suite "Settings read tests":
  echo "Starting Settings Read Tests"

  setup:
    let TEST_SETTINGS_PATH = joinPath(getCurrentDir(),"tests", "test_settings/test_settings.toml")

  test "Can Load settings":
    let rawTestSettings = readFile(TEST_SETTINGS_PATH)
    var parsedSettings: Option[FFTSettings] = loadSettings(rawTestSettings)

    assert parsedSettings.isSome

    require(parsedSettings.get().title == "Test Settings")
    require(parsedSettings.get().windowSize[0] == 800)
    require(parsedSettings.get().windowSize[1] == 600)

    require(parsedSettings.get().video == 32)
