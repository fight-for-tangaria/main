import ecs


type
  MyComponent1 = object
    x, y: int
  MyComponent2 = object
    opponent: Entity
    inRange: bool

proc main() =
  let w = newWorld()
  let e1 = w.newEntity()
  let e4 = w.newEntity()

  e1.addComponent(MyComponent1(x: 1, y: 5))

  e4.addComponent(MyComponent1(x: 7, y: 77))

  var myc = MyComponent2(opponent: e4, inRange: true)
  e1.addComponent(myc)
  e4.addComponent(MyComponent2(opponent: e1, inRange: true))


  w.prepareForProcessing()

  var processedEntities = 0

  var
    e1x = e1.getComponentPtr(MyComponent1).x
    e4x = e4.getComponentPtr(MyComponent1).x
    e1y = e1.getComponentPtr(MyComponent1).y
    e4y = e4.getComponentPtr(MyComponent1).y

  while e1x != e4x and e1y != e4y:
    block:
      proc process(c: var MyComponent1, res: var MyComponent2)  =
          ## This proc is basically the System
          echo "In CB"
          let pos = res.opponent.getComponentPtr(MyComponent1)
          if c.x > pos.x:
            if pos.x - c.x < 2:
              res.inRange = true
            dec c.x
          elif c.x < pos.x:
            if c.x - pos.x < 2:
              res.inRange = true
            inc c.x

          if c.y > pos.y:
            if pos.y - c.y < 2:
              res.inRange = true
            dec c.y
          elif c.y < pos.y:
            if c.y - pos.y < 2:
              res.inRange = true
            inc c.y

          if res.inRange:
            let opponent = res.opponent.getComponentPtr(MyComponent2)
            opponent.inRange = true
          inc processedEntities

      w.forEveryMatchingEntity(process)
      e1x = e1.getComponentPtr(MyComponent1).x
      e4x = e4.getComponentPtr(MyComponent1).x
      e1y = e1.getComponentPtr(MyComponent1).y
      e4y = e4.getComponentPtr(MyComponent1).y

when isMainModule:
  main()
