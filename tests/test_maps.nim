import unittest, os

import renderer/assets/maps

suite "Tests Map Loading":

  setup:
    let mapCellTestMapPath = joinPath(getCurrentDir(),"tests", "test_maps/test_map1.mf")
    let mapLocationCellTestMapPath = joinPath(getCurrentDir(),"tests", "test_maps/test_map1.mfc")
    let terraintTestMapPath = joinPath(getCurrentDir(),"tests", "test_maps/test_map1.tf")

  test "Build a map":
    let map = buildMap(2, 1, "Test Map", mapLocationCellTestMapPath, terraintTestMapPath, mapCellTestMapPath)

    require map.title == "Test Map"
    require map.width == 2
    require map.height == 1
    require map.cells[0].id == 1337
    require $map.cells[0].terrain == $ttPlains

    require map.locations[0].cells[0] == 1337
    let
      expectedEvent = MapCellEvent(eventCode: 1)
      expectedEntity = MapCellEntity(isHostile: 0, entityCode: 1)
      expectedStructure = MapCellStructure(isHostile: 0, structureCode: 1)

    require map.locations[0].events[0] == expectedEvent
    require map.locations[0].entities[0] == expectedEntity
    require map.locations[0].structures[0] == expectedStructure

  test "Render MapCell":
    discard ""
