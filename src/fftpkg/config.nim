#Defaults
const DEFAULT_SETTINGS_PATH = "../../settings/settings.toml"
const DEFAULT_WINDOW_SIZE = (800, 600)

export DEFAULT_SETTINGS_PATH, DEFAULT_WINDOW_SIZE
