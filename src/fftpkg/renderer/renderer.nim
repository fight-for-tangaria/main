import csfml, options, sugar, sequtils, strutils, tables
import utilities/readers
import lib


const
  GRID_WIDTH = 64
  GRID_HEIGHT = 64
  TERRAIN_TO_FILENAME = {
    $TerrainType.ttPlains: "plains",
    $TerrainType.ttForest: "forest",
    $TerrainType.ttMountain: "mountain",
    $TerrainType.ttRiver: "river",
    $TerrainType.ttLake: "lake",
    $TerrainType.ttOcean: "ocean"
  }.toTable


type
  Grid = object
    rows, cols: int
    cells: seq[seq[MapCellSprite]]


  MapCellSprite = Sprite


proc newGrid(): Grid = 
  result = Grid()
  result.cells = @[]
  result.rows = 0
  result.cols = 0


proc newWindow(): RenderWindow =
  ## Create a new window
  let video_mode = video_mode(SETTINGS.windowSize[0], SETTINGS.windowSize[1], SETTINGS.video)
  result = new_RenderWindow(video_mode, SETTINGS.title)
  result.vertical_sync_enabled = true


proc createSpriteFromTexture(texture: Texture, origin, scale, position: Vector2f): Sprite = 
  ## Get a texture based on the terrain
  result = newSprite(texture)
  result.origin = origin
  result.scale = scale
  result.position = position


proc getTerrainTexture(terrain: TerrainType): (Texture, Vector2i) =
  ## Get a texture based on the terrain
  let texture = new_Texture("assets/tile_$#.png" % [TERRAIN_TO_FILENAME[$terrain]])
  let sz = texture.size
  return (texture, sz)


proc calcGridXOffset(x: int, scale: Vector2f, size: Vector2i): float =
  ## Calc the X offset on the grid based on an offset
  echo (x * size.x).float * scale.x
  (x * size.x).float * scale.x


proc calcGridYOffset(y: int, scale: Vector2f, size: Vector2i): float =
  ## Calc the X offset on the grid based on an offset
  (y * size.y).float * scale.y


proc newMapCellSprite(cell: MapCell, window: RenderWindow, gridPosition: Vector2i, addOverlay: bool = false): MapCellSprite = 
  ##Create a Sprite from a Map Cell

  let 
    (texture, sz) = getTerrainTexture(cell.terrain)

  let 
    origin = vec2(sz.x / 5, sz.y / 5)
    scale = vec2(1.0, 1.0)
    position = vec2(window.size.x / 5 + calcGridXOffset(gridPosition.x, scale, sz), window.size.y / 5 + calcGridYOffset(gridPosition.y, scale, sz))

  if not addOverlay:
    return createSpriteFromTexture(
      texture = texture,
      origin = origin,
      scale = scale,
      position = position
    )

  var 
    origOrigin = vec2(0, 0)
    origScale = vec2(1.0, 1.0)
    origPosition = vec2(0, 0)
    backTexture = new_Texture("assets/tile_base.png")
    backsz = backTexture.size
    renderTexture = newRenderTexture(width = cint(backsz.x.float * origScale.x), height = cint(backsz.y.float * origScale.x), depthBuffer = false)

  renderTexture.clear(color(0, 0, 0))
  renderTexture.drawSprite(
    createSpriteFromTexture(
      texture = backTexture,
      origin = origOrigin,
      scale = origScale,
      position = origPosition
    ),
    renderStates()
  )

  renderTexture.drawSprite(
    createSpriteFromTexture(
      texture = texture,
      origin = origOrigin,
      scale = vec2(0.75, 0.75),
      position = vec2(backsz.x.float / 3.5, backsz.y.float / 3.5)
    ),
    renderStates()
  )
  texture.destroy()
  backTexture.destroy()

  renderTexture.display()

  result = createSpriteFromTexture(
    texture = renderTexture.texture,
    origin = origin,
    scale = scale,
    position = position
  )


when isMainModule:
  import private/test_utils

  echo "Render Tests"

  assert MapCell is SomeMapType
  var
    cell, cell2: MapCell
    #cellFactory = newFactory[MapCell]()
    cellFactory = new Factory[MapCell]
    locationFactory = newFactory[MapLocation]()
    cells = toSeq(0 .. 20).map(proc(x: int): MapCell = 
      cellFactory.create(random = true)
    )

  cell = cellFactory.create(random = true)
  var 
    window = newWindow()
    counter = 0

  #Turn our cells into sprites
  var sprites = cells.map(proc(c: MapCell): Sprite =
    counter.inc
    newMapCellSprite(c, window, vec2(counter, counter))
    )

  while window.open:
    var event: Event
    while window.poll_event(event):
      case event.kind
        of EventType.Closed:
          window.close()
        of EventType.KeyPressed:
          case event.key.code:
            of KeyCode.Escape:
              window.close()
            else:
              discard
        else: discard

    window.clear color(0, 0, 0)
    
    #Draw our sprites
    for sprite in sprites:
      window.draw sprite

    window.display()
  
  #Clean up
  for sprite in sprites:
    let texture = sprite.texture
    sprite.destroy()
    texture.destroy()
  window.destroy()
