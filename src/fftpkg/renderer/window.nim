include lib
import csfml/[graphics, window]

proc initWindow*(): RenderWindow = 
  let 
    windowStyle = WindowStyle.Titlebar | WindowStyle.Resize | WindowStyle.Close
    vm = videoMode(SETTINGS.windowSize[0].cint, SETTINGS.windowSize[1].cint)

  result = newRenderWindow(vm, SETTINGS.title, windowStyle)
  result.verticalSyncEnabled = true
