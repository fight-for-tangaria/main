import os, options, strutils, streams, strformat, iterutils, sequtils

include lib
import utilities/caching

proc getMapDirs(): Option[tuple[mapPath: string, terrainPath: string, attributesPath: string]]

let MAP_DIRS_OPT = getMapDirs()

var MAP_DIRS: tuple[mapPath: string, terrainPath: string, attributesPath: string] 
if isSome(MAP_DIRS_OPT):
  MAP_DIRS = MAP_DIRS_OPT.get()


proc getMapConfigDir(): string =
  joinPath(getConfigDir(), "FightForTang", "Maps")


proc getBaseMapPath(): string = 
  joinPath(getMapConfigDir(), "maps")


proc getBaseTerrainPath(): string =
  joinPath(getMapConfigDir(), "terrains")


proc getBaseAttributesPath(): string =
  joinPath(getMapConfigDir(), "attributes")


proc getMapPath(name: string): string =
  ## File path for a file defining map location flags
  ##
  ## Attributes are stored in the format of Map Location Flags,
  ## where each 8 bits represents a single flag
  ## Attributes are stores in the following order:
  ## Cells - The Cell ID's that make up a location (max 8x8). Uses left 0 padded 32 bits for each id
  ## Events - Max 10
  ## Entities - Max 50
  ## Structures - Max 8
  ## Each set type is delimited by a semicolon (;) and each set of sets is split by a newline
  ## See the definition of MapLocation{Event, Entitity, Structure} for how these are packed
  ## See the definition of {entity, event and structure}.nim
  joinPath(getBaseMapPath(), name)


proc getTerrainPath(name: string): string =
  ## File path for a file defining terrains for cells
  ##
  ## Each cell's terrain can be of a certain TerrainType
  ## Check the string values for each enum for a mapping
  joinPath(getBaseTerrainPath(), name)


proc getAttributesPathForMap(name: string): string =
  ## File path for a file defining attributes
  ##
  ## Attributes are stored in the format of Map Cell Flags,
  ## where each 8 bits represents a single flag
  ## Attributes are stores in the following order:
  ## Id - An easy ID to reference cells with
  ## Events - Max 100
  ## Entities - Max 100
  ## Structures - Max 10
  ## Each type of set is delimited by a semicolon (;) and each set of sets is delimited by a line
  ## See the definition of MapCell{Event, Entitity, Structure} for how these are packed
  ## See the definition of {entity, event and structure}.nim
  joinPath(getBaseAttributesPath(), name)


proc getMapDirs(): Option[tuple[mapPath: string, terrainPath: string, attributesPath: string]] =
  ## Gets directory path for maps, terrains and attribute files
  let mapPath = getMapConfigDir()

  if not dirExists(parentDir(parentDir(mapPath))):
    createDir(parentDir(mapPath))

  if not dirExists(parentDir(mapPath)):
    createDir(parentDir(mapPath))

  if not fileExists(mapPath):
    return none(tuple[mapPath: string, terrainPath: string, attributesPath: string])
  
  try:
    return some((mapPath: getBaseMapPath(), terrainPath: getBaseTerrainPath(), attributesPath: getBaseAttributesPath()))
  except IOError:
    error("No map file found")
    return none(tuple[mapPath: string, terrainPath: string, attributesPath: string])

# Iterators and parsers

iterator splitStringBySize(str: string, size: int, max: int): (int, string) =
  if str.len mod size != 0:
    error(fmt"string len {str.len} and size {size}")
    raise newException(IOError, fmt"Invalid alignment for string with split of size {size}")
  elif str.len div size > max:
    raise newException(IOError, fmt"Too many splits: {max}")

  var index, counter = 0
  while index < str.len and index + 31 < str.len:
    yield (counter, str[0 + index .. 31 + index])
    counter += 1
    index += 32

# Map Location splitters

proc splitMapCellIds(cells: string): array[MAP_LOCATION_MAX_CELL_SIZE, uint32] =
  ## Each Map Cell ID is a 32 bit integer, so we want to make sure it's properly aligned first

  var index = 0
  for (idx, id) in cells.splitStringBySize(32, MAP_LOCATION_MAX_CELL_SIZE):
    result[idx] = parseBinInt(cells[0 + index .. 31 + index]).uint32
    index += 32

proc splitLocationEvents(events: string): array[MAP_LOCATION_MAX_EVENTS, cuint] =
  ## Each event is an 8 bit uint
  var index = 0
  for (idx, id) in events.splitStringBySize(8, MAP_LOCATION_MAX_EVENTS):
    result[idx] = parseBinInt(events[0 + index .. 8 + index]).cuint
    index += 8

proc splitLocationEntities(events: string): array[MAP_LOCATION_MAX_ENTITIES, cuint] =
  ## Each entity is an 8 bit uint
  var index = 0
  for (idx, id) in events.splitStringBySize(8, MAP_LOCATION_MAX_ENTITIES):
    result[idx] = parseBinInt(events[0 + index .. 8 + index]).cuint
    index += 8

proc splitLocationStructures(events: string): array[MAP_LOCATION_MAX_STRUCTURES, cuint] =
  ## Each structure is an 8 bit uint
  var index = 0
  for (idx, id) in events.splitStringBySize(8, MAP_LOCATION_MAX_STRUCTURES):
    result[idx] = parseBinInt(events[0 + index .. 8 + index]).cuint
    index += 8

# Map Cell splitters

proc splitEvents(events: string): array[MAP_CELL_MAX_EVENTS, cuint] =
  ## Each event is an 8 bit uint
  var index = 0
  for (idx, id) in events.splitStringBySize(8, MAP_CELL_MAX_EVENTS):
    result[idx] = parseBinInt(events[0 + index .. 8 + index]).cuint
    index += 8

proc splitEntities(events: string): array[MAP_CELL_MAX_ENTITIES, cuint] =
  ## Each entity is an 8 bit uint
  var index = 0
  for (idx, id) in events.splitStringBySize(8, MAP_CELL_MAX_ENTITIES):
    result[idx] = parseBinInt(events[0 + index .. 8 + index]).cuint
    index += 8

proc splitStructures(events: string): array[MAP_CELL_MAX_STRUCTURES, cuint] =
  ## Each structure is an 8 bit uint
  var index = 0
  for (idx, id) in events.splitStringBySize(8, MAP_CELL_MAX_STRUCTURES):
    result[idx] = parseBinInt(events[0 + index .. 8 + index]).cuint
    index += 8

proc newMapCell(id: int32, terrain: TerrainType, flags: MapCellFlags): MapCell =
  new result
  result.id = id
  result.terrain = terrain
  result.flags = flags

proc mapLocationChunks(file: File): iterator: MapLocationChunk =
  ## Returns MapLocationChunks 

  return iterator(): MapLocationChunk =
    var 
      mapStream = newFileStream(file)
    defer: mapStream.close()

    for line in mapStream.lines():
      let 
        tokens = line.replace("#", "").split(";") #Remove comment lines and tokenrize
        cells = tokens[0]
        events = tokens[1]
        entities = tokens[2]
        structures = tokens[3]

      yield MapLocationChunk(
        cells: cells.splitMapCellIds(),
        events: events.splitLocationEvents(),
        entities: entities.splitLocationEntities(),
        structures: structures.splitLocationStructures()
      )

proc mapChunks(file: File): iterator: MapCellChunk =
  ## Returns a closure to an iterator that returns Map Cell Chunks

  return iterator(): MapCellChunk =
    var 
      mapStream = newFileStream(file)

    for line in mapStream.lines():
      let 
        tokens = line.replace("#", "").split(";")
      let 
        id = tokens[0]
        events = tokens[1]
        entities = tokens[2]
        structures = tokens[3]

      yield MapCellChunk(
        id: fromBin[int32](id),
        events: events.splitEvents(),
        entities: entities.splitEntities(),
        structures: structures.splitStructures()
      )


proc terrainTypes(file: File): iterator: TerrainType =
  ## Loads terrain types

  return iterator(): TerrainType = 
    var 
      terrainStream = newFileStream(file)
    defer: terrainStream.close()

    while terrainStream.peekChar() != '\x00':
      let c = terrainStream.readChar()
      try:
        yield parseEnum[TerrainType]($c)
      except ValueError:
        error(fmt"Invalid value {c} in terrain file load")
        raise 


proc newMapCellFlags(cellData: MapCellChunk): MapCellFlags =
  let 
    events = map(cellData.events, proc(x: cuint): MapCellEvent =
      MapCellEvent(eventCode: x))
    entities = map(cellData.entities, proc(x: cuint): MapCellEntity =
      MapCellEntity(isHostile: x shr 7, entityCode: x and 0b01111111))
    structures = map(cellData.structures, proc(x: cuint): MapCellStructure =
      MapCellStructure(isHostile: x shr 7, structureCode: x and 0b01111111))

  var 
    eventsArray: array[MAP_CELL_MAX_EVENTS, MapCellEvent]
    entitiesArray: array[MAP_CELL_MAX_ENTITIES, MapCellEntity]
    structuresArray: array[MAP_CELL_MAX_STRUCTURES, MapCellStructure]
    flagCounter = 0

  for event in events: 
    eventsArray[flagCounter] = event
    flagCounter.inc

  result.events = eventsArray

  flagCounter = 0
  for entity in entities:
    entitiesArray[flagCounter] = entity
    flagCounter.inc
  
  result.entities = entitiesArray

  flagCounter = 0
  for structure in structures:
    structuresArray[flagCounter] = structure
    flagCounter.inc

  result.structures = structuresArray


proc buildMap*(width, height: int, name, mapFilePath, terrainFilePath, attributesFilePath: string): Map =
  let 
    mapFile = open(mapFilePath)
    terrainFile = open(terrainFilePath)
    attributesFile = open(attributesFilePath)

  new result

  var 
    cellCounter = 0
  # Zip up all our data so we have a tuple of map location to map chunk in `mapData` and terrain in `terrain`
  for (mapCell, terrainType) in zip(attributesFile.mapChunks(), terrainFile.terrainTypes()):

    let flags = newMapCellFlags(mapCell)
    result.cells[cellCounter] = newMapCell(id = mapCell.id, terrain = terrainType, flags = flags)

  result.title = name
  result.width = width
  result.height = height


proc buildMap*(name: string, width, height: int): Map =
  ## Given a map name, build a Map
  let
    mapFilePath = MAP_DIRS.mapPath / name / ".mf"
    terrainFilePath = MAP_DIRS.terrainPath / name / ".tf"
    attributesFilePath = MAP_DIRS.attributesPath / name / ".mfc"

  if not fileExists(mapFilePath) or not fileExists(terrainFilePath) or not fileExists(attributesFilePath):
    raise newException(IOError, fmt"Files do not all exist:\n{mapFilePath}\n{terrainFilePath}\n{attributesFilePath}")

  buildMap(width, height, name, mapFilePath, terrainFilePath, attributesFilePath)
