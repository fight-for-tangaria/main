import config
import parsetoml
import options, strformat

const WINDOW_DIMENSIONS_KEY = "window"

type 
  FFTSettings* = ref FFTSettingsObj
  FFTSettingsObj = object
    ## Contains settings for the application
    title*: string
    windowSize*: (cint, cint)
    video*: (cint)

  InvalidTomlSettingsException* = object of Exception ## \
    ## Indicates that your settings toml was invalid

  ImproperlyConfiguredSettingsException* = object of Exception ## \
    ## Indicates that you have an invalid settings configuration

  SettingsFileNotFoundException* = object of Exception ## \
    ## Indicates that no settings file was found. Check config.nim for the default location

const RAW_SETTINGS: string = try: staticRead(DEFAULT_SETTINGS_PATH)
                 except IOError: raise newException(SettingsFileNotFoundException, fmt"No settings file found at {DEFAULT_SETTINGS_PATH}")

proc loadSettings*(settings: string): Option[FFTSettings] = 
  ## Returns a FFTSettings by loading a given string
  let parsedSettings = try: parsetoml.parseString(settings)
                        except TomlError:
                          raise newException(InvalidTomlSettingsException, "Error parsing TOML")

  let (defaultWidth, defaultHeight) = DEFAULT_WINDOW_SIZE

  var windowSize = (
    parsedSettings[WINDOW_DIMENSIONS_KEY]["width"].getInt(defaultWidth).cint,
    parsedSettings[WINDOW_DIMENSIONS_KEY]["height"].getInt(defaultHeight).cint
  )
  defer:
    windowSize = (defaultWidth.cint, defaultHeight.cint)

  let title = parsedSettings["title"].getStr()
  let pixelDepth = parsedSettings["game"]["video"]["pixelDepth"].getInt(32).cint
  return some(FFTSettings(title: title, windowSize: windowSize, video: pixelDepth))

let SETTINGS*: FFTSettings = loadSettings(RAW_SETTINGS).get()
