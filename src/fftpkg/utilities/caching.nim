import os, options, strformat, streams, json, zip/gzipfiles, sequtils, bitops
include lib

proc readAllAndClose(f: Stream): string =
  shallowCopy result, f.readAll()
  f.close()


proc getOrCreateCacheDir(): string =
  result = joinPath(getConfigDir(), "FightForTang", "cache")
  discard existsOrCreateDir(result)


proc getOrCreateMapBaseCacheDir(): string =
  result = joinPath(getOrCreateCacheDir(), "maps")
  discard existsOrCreateDir(result)

proc getMapFilePath(name: string): string = 
  joinPath(getOrCreateMapBaseCacheDir(), name & ".map.gz")

proc setMapCache(map: Map) =
  ## Writes a map to a gzipped file
  var fd = newGzFileStream(getMapFilePath(map.title), fmWrite)
  let chunk_size = 128

  let 
    data = %* map
    mappedData = $data

  var 
    idx = 0
    num_cuints = mappedData.len

  while true:
    fd.writeData(mappedData[idx].unsafeAddr, min(num_cuints, chunk_size))
    if num_cuints < chunk_size:
      break
    dec(num_cuints, chunk_size)
    inc(idx, chunk_size)
  fd.close()

proc unpackMap(node: JsonNode): Map =
  ## Unpacks a JsonNode into a map

  var
    eventsArray: array[MAP_CELL_MAX_EVENTS, MapCellEvent]
    entitiesArray: array[MAP_CELL_MAX_ENTITIES, MapCellEntity]
    structuresArray: array[MAP_CELL_MAX_STRUCTURES, MapCellStructure]

    eventsLocationsArray: array[MAP_LOCATION_MAX_EVENTS, MapLocationEvent]
    entitiesLocationsArray: array[MAP_LOCATION_MAX_ENTITIES, MapLocationEntity]
    structuresLocationsArray: array[MAP_LOCATION_MAX_STRUCTURES, MapLocationStructure]
        
    locations: array[MAX_MAP_LOCATION_SIZE, MapLocation]
    mapcells: array[MAX_MAP_SIZE, MapCell]
    locationCellIds: array[MAP_LOCATION_MAX_CELL_SIZE, int32]

  var outerCount = 0
  assert node["cells"].getElems().len <= MAX_MAP_SIZE
  #Build map cells
  for cell in node["cells"].getElems():
    assert cell["events"].getElems().len <= MAP_CELL_MAX_EVENTS
    let eventElements = cell["events"].getElems()
    for eventIdx in 0 ..< MAP_CELL_MAX_EVENTS:
      if eventIdx >= eventElements.len: 
        eventsArray[eventIdx] = MapCellEvent(eventCode: 0)
      else:
        let event = eventElements[eventIdx]
        eventsArray[eventIdx] = MapCellEvent(eventCode: event["eventCode"].getInt().cuint)

    assert cell["entities"].getElems().len <= MAP_CELL_MAX_ENTITIES
    let entityElements = cell["entities"].getElems()
    for entityIdx in 0 ..< MAP_CELL_MAX_ENTITIES:
      if entityIdx >= entityElements.len: 
        entitiesArray[entityIdx] = MapCellEntity(isHostile: 0, entityCode: 0)
      else:
        let entity = entityElements[entityIdx]
        entitiesArray[entityIdx] = MapCellEntity(isHostile: entity["isHostile"].getInt().cuint, entityCode: entity["entityCode"].getInt().cuint)

    assert cell["structures"].getElems().len <= MAP_CELL_MAX_STRUCTURES
    let structureElements = cell["structures"].getElems()
    for structureIdx in 0 ..< MAP_CELL_MAX_STRUCTURES:
      if structureIdx >= structureElements.len: 
        structuresArray[structureIdx] = MapCellStructure(isHostile: 0, structureCode: 0)
      else:
        let structure = structureElements[structureIdx]
        structuresArray[structureIdx] = MapCellStructure(isHostile: structure["isHostile"].getInt().cuint, structureCode: structure["structureCode"].getInt().cuint)

    mapCells[outerCount] = MapCell(id: cell["id"].getInt().int32, terrain: TerrainType(cell["terrain"].getInt()), 
      flags: MapCellFlags(events: eventsArray, entities: entitiesArray, structures: structuresArray)
      )

    outerCount.inc


  outerCount = 0
  for location in node["locations"].getElems():
    assert location["cellIds"].getElems().len <= MAX_MAP_SIZE
    let cellIds = location["cellIds"].getElems()
    for cellIdx in 0 ..< MAX_MAP_SIZE:
      if cellIdx >= cellIds.len: 
        locationCellIds[cellIdx] = 0
      else:
        locationCellIds[cellIdx] = cellIds[cellIdx].getInt().int32

    assert location["events"].getElems().len <= MAP_location_MAX_EVENTS
    let eventElements = location["events"].getElems()
    for eventIdx in 0 ..< MAP_location_MAX_EVENTS:
      if eventIdx >= eventElements.len: 
        eventsLocationsArray[eventIdx] = MaplocationEvent(eventCode: 0)
      else:
        let event = eventElements[eventIdx]
        eventsLocationsArray[eventIdx] = MaplocationEvent(eventCode: event["eventCode"].getInt().cuint)

    assert location["entities"].getElems().len <= MAP_location_MAX_ENTITIES
    let entityElements = location["entities"].getElems()
    for entityIdx in 0 ..< MAP_location_MAX_ENTITIES:
      if entityIdx >= entityElements.len: 
        entitiesLocationsArray[entityIdx] = MaplocationEntity(isHostile: 0, entityCode: 0)
      else:
        let entity = entityElements[entityIdx]
        entitiesLocationsArray[entityIdx] = MaplocationEntity(isHostile: entity["isHostile"].getInt().cuint, entityCode: entity["entityCode"].getInt().cuint)

    assert location["structures"].getElems().len <= MAP_location_MAX_STRUCTURES
    let structureElements = location["structures"].getElems()
    for structureIdx in 0 ..< MAP_LOCATION_MAX_STRUCTURES:
      if structureIdx >= structureElements.len: 
        structuresLocationsArray[structureIdx] = MaplocationStructure(isHostile: 0, structureCode: 0)
      else:
        let structure = structureElements[structureIdx]
        structuresLocationsArray[structureIdx] = MaplocationStructure(isHostile: structure["isHostile"].getInt().cuint, structureCode: structure["structureCode"].getInt().cuint)

    locations[outerCount] = MapLocation(
      cells: locationCellIds,
      events: eventsLocationsArray,
      entities: entitiesLocationsArray,
      structures: structuresLocationsArray
    )

  result.title = node["title"].getStr
  result.width = node["width"].getInt
  result.height = node["height"].getInt
  result.cells = mapCells
  result.locations = locations


proc getMapCache(name: string): Map =
  echo "Getting data"
  let data = newGzFileStream(getMapFilePath(name)).readAllAndClose()
  echo "Parsing data"
  echo data
  let js = parseJson(data)
  echo "Parse json"
  echo js.len
  js.unpackMap()


proc buildTestMap(title: string, width, height, size: int, cellEvents: seq[cuint], terrain: uint8,
              cellEntities: seq[cuint], cellStructures: seq[cuint],
              locationCellIds: seq[int32], locationEvents: seq[cuint],
              locationEntities: seq[cuint], locationStructures: seq[cuint]): Map =
  ## Builds a Map for testing

  echo "Starting map build for test"
  var
    eventsArray: array[MAP_CELL_MAX_EVENTS, MapCellEvent]
    entitiesArray: array[MAP_CELL_MAX_ENTITIES, MapCellEntity]
    structuresArray: array[MAP_CELL_MAX_STRUCTURES, MapCellStructure]

    eventsLocationsArray: array[MAP_LOCATION_MAX_EVENTS, MapLocationEvent]
    entitiesLocationsArray: array[MAP_LOCATION_MAX_ENTITIES, MapLocationEntity]
    structuresLocationsArray: array[MAP_LOCATION_MAX_STRUCTURES, MapLocationStructure]
        
    locations: array[MAX_MAP_LOCATION_SIZE, MapLocation]
    mapCells: array[MAX_MAP_SIZE, MapCell]
    locationCellIds: array[MAP_LOCATION_MAX_CELL_SIZE, int32]

  echo "cell events"
  assert cellEvents.len <= MAP_CELL_MAX_EVENTS
  for eventIdx in 0 ..< MAP_CELL_MAX_EVENTS:
    if eventIdx >= cellEvents.len: 
      eventsArray[eventIdx] = MapCellEvent(eventCode: 0)
    else:
      eventsArray[eventIdx] = MapCellEvent(eventCode: cellEvents[eventIdx])

  echo "cell entities"
  assert cellEntities.len <= MAP_CELL_MAX_ENTITIES
  for entityIdx in 0 ..< MAP_CELL_MAX_ENTITIES:
    if entityIdx >= cellEntities.len: 
      entitiesArray[entityIdx] = MapCellEntity(isHostile: 0, entityCode: 0)
    else:
      let event: cuint = cellEntities[entityIdx]
      entitiesArray[entityIdx] = MapCellEntity(isHostile: bitand(event, 0x128), entityCode: bitand(event, 1))

  echo "cell structures"
  assert cellStructures.len <= MAP_CELL_MAX_STRUCTURES
  for structureIdx in 0 ..< MAP_CELL_MAX_STRUCTURES:
    if structureIdx >= cellStructures.len: 
      structuresArray[structureIdx] = MapCellStructure(isHostile: 0, structureCode: 0)
    else:
      let structure = cellStructures[structureIdx]
      structuresArray[structureIdx] = MapCellStructure(isHostile: bitand(structure, 0x128), structureCode: bitand(structure, 1)) 

  mapCells[0] = MapCell(id: 0, terrain: TerrainType(terrain), 
    flags: MapCellFlags(events: eventsArray, entities: entitiesArray, structures: structuresArray)
    )



  echo "cell loc ids"
  assert locationCellIds.len <= MAX_MAP_SIZE
  for cellIdx in 0 ..< MAX_MAP_SIZE:
    if cellIdx >= locationCellIds.len: 
      locationCellIds[cellIdx] = 0
    else:
      locationCellIds[cellIdx] = locationCellIds[cellIdx]

  echo "cell loc events"
  assert locationEvents.len <= MAP_location_MAX_EVENTS
  for eventIdx in 0 ..< MAP_location_MAX_EVENTS:
    if eventIdx >= locationEvents.len: 
      eventsLocationsArray[eventIdx] = MapLocationEvent(eventCode: 0)
    else:
      eventsLocationsArray[eventIdx] = MapLocationEvent(eventCode: locationEvents[eventIdx])

  echo "cell loc entities"
  assert locationEntities.len <= MAP_location_MAX_ENTITIES
  for entityIdx in 0 ..< MAP_location_MAX_ENTITIES:
    if entityIdx >= locationEntities.len: 
      entitiesLocationsArray[entityIdx] = MapLocationEntity(isHostile: 0, entityCode: 0)
    else:
      let entity = locationEntities[entityIdx]
      entitiesLocationsArray[entityIdx] = MapLocationEntity(isHostile:  bitand(entity, 0x128), entityCode: bitand(entity, 1))

  echo "cell loc structures"
  assert locationStructures.len <= MAP_LOCATION_MAX_STRUCTURES
  for structureIdx in 0 ..< MAP_location_MAX_STRUCTURES:
    if structureIdx >= locationStructures.len: 
      structuresLocationsArray[structureIdx] = MapLocationStructure(isHostile: 0, structureCode: 0)
    else:
      let structure = locationStructures[structureIdx]
      structuresLocationsArray[structureIdx] = MapLocationStructure(isHostile: bitand(structure, 0x128), structureCode: bitand(locationStructures[structureIdx], 1))

  locations[0] = MapLocation(
    cells: locationCellIds,
    events: eventsLocationsArray,
    entities: entitiesLocationsArray,
    structures: structuresLocationsArray
  )

  result.title = title
  result.width = width
  result.height = height
  result.cells = mapCells
  result.locations = locations

when isMainModule:
  echo "In main"
  var
    map: Map = buildTestMap(
      title = "Test", width = 1, height = 1, size = 1, cellEvents = newSeq[cuint](1.cuint), terrain = 1,
      cellEntities = newSeq[cuint](1.cuint), cellStructures = newSeq[cuint](1.cuint),
      locationCellIds = newSeq[int32](1.int32), locationEvents = newSeq[cuint](1.cuint),
      locationEntities = newSeq[cuint](1.cuint), locationStructures = newSeq[cuint](1.cuint)
    )

  echo "Setting cache"
  setMapCache(map)
  echo "Set cache"
  let title = map.title
  #let
  #  data = newGzFileStream(getMapFilePath(title)).readAllAndClose()
  #  newMap = to(parseJson(data), Map)

  #assert newMap.title == map.title
  #assert newMap.width == map.width
  #assert newMap.height == map.height
  #assert newMap.size == map.size
  #assert newMap.cells == map.cells
  echo "Caching got"
  let cachedMap = getMapCache(title)
  assert map == cachedMap
