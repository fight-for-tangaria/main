import logging, os, strformat
import utilities/readers, messaging/components

# Map Config
discard existsOrCreateDir(getConfigDir() / "FightForTang",)

const MAX_MAP_WIDTH* = 1600
const MAX_MAP_HEIGHT* = 1200
const MAX_MAP_SIZE* = MAX_MAP_HEIGHT * MAX_MAP_WIDTH

# Map Cell Config

const MAP_CELL_MAX_EVENTS* = 100
const MAP_CELL_MAX_ENTITIES* = 100
const MAP_CELL_MAX_STRUCTURES* = 10
const MAX_MAP_LOCATION_SIZE* = 100

# Map Location Config
const MAP_LOCATION_MAX_CELL_SIZE* = 64
const MAP_LOCATION_MAX_CELL_HEIGHT* = 8
const MAP_LOCATION_MAX_CELL_WIDTH = 8
const MAP_LOCATION_MAX_EVENTS* = 10
const MAP_LOCATION_MAX_ENTITIES* = 50
const MAP_LOCATION_MAX_STRUCTURES* = 8

discard existsOrCreateDir("logs")
var fileLog = newFileLogger("errors.log", levelThreshold=lvlDebug)
var consoleLogger = newConsoleLogger()

addHandler(fileLog)
addHandler(consoleLogger)

type
  TerrainType* = enum
    ttPlains = "=",
    ttForest = "#",
    ttMountain = "^",
    ttRiver = "~",
    ttLake = "*",
    ttOcean = "@"

  Map* = ref MapObj
  MapObj = object
    ## A representation of a map
    ## 
    ## Each cell contains information about any events occuring in it
    ## and information describing the entities and structures currently on it
    ## These are the components of a cell (event, entity, structure)
    ## 
    ## Each location is a collection of cells with it's own set of components
    ## These components typically represent visible game objects
    title*: string
    width*: int
    height*: int
    size*: int
    cells*: array[MAX_MAP_SIZE, MapCell]
    locations*: array[MAX_MAP_LOCATION_SIZE, MapLocation]

  MapCellEvent* = object
    ## A code for an event occurring in a cell
    eventCode*: cuint

  MapCellEntity* = object
    ## Represents a single entity and quick access check for whether it's hostile
    isHostile* {.bitsize:1}: cuint
    entityCode* {.bitsize:7.}: cuint

  MapCellStructure* = object
    ## Represents a single structure and quick access check for whether it's hostile
    isHostile* {.bitsize:1}: cuint
    structureCode* {.bitsize:7.}: cuint

  MapLocationEvent* = MapCellEvent
  MapLocationEntity* = MapCellEntity
  MapLocationStructure* = MapCellStructure

  SomeMapEvent* = MapCellEvent | MapLocationEvent 
  SomeMapEntity* = MapCellEntity | MapLocationEntity
  SomeMapStructure* = MapCellStructure | MapLocationStructure

  SomeMapType* = MapCell or MapLocation

  MapCellFlags* = object
    ## Flags for individual cells on the map
    ## Uses a single cuint to check and 
    events*: array[MAP_CELL_MAX_EVENTS, MapCellEvent]
    entities*: array[MAP_CELL_MAX_ENTITIES, MapCellEntity]
    structures*: array[MAP_CELL_MAX_STRUCTURES, MapCellStructure]
  
  MapCell* = ref object of RootObj
    ## An individual cell on the map
    ## terrain contains the type of terrain (see terrains.nim)
    ## flags contains all active flags, such as entities, events and structures
    id*: int32
    terrain*: TerrainType
    flags*: MapCellFlags

  MapLocation* = ref object of RootObj
    ## A location on a map, which may consist of multiple cells
    cells*: array[MAP_LOCATION_MAX_CELL_SIZE, int32] #MapCell Ids
    events*: array[MAP_LOCATION_MAX_EVENTS, MapLocationEvent]
    entities*: array[MAP_LOCATION_MAX_ENTITIES, MapLocationEntity]
    structures*: array[MAP_LOCATION_MAX_STRUCTURES, MapLocationStructure]

  MapChunk = object
    mapData: MapLocation
    attributesData: MapCell

  MapCellChunk = object
    ## For reading map cells from files
    id: int32
    events: array[MAP_CELL_MAX_EVENTS, cuint]
    entities: array[MAP_CELL_MAX_ENTITIES, cuint]
    structures: array[MAP_CELL_MAX_STRUCTURES, cuint]

  MapLocationChunk = object
    ## For reading map locations from files
    cells: array[MAP_LOCATION_MAX_CELL_SIZE, uint32]
    events: array[MAP_LOCATION_MAX_EVENTS, cuint]
    entities: array[MAP_LOCATION_MAX_ENTITIES, cuint]
    structures: array[MAP_LOCATION_MAX_STRUCTURES, cuint]


proc `$`*(flags: MapCellFlags): string = 
  fmt"events: {$flags.events} entities: {$flags.entities} structures: {$flags.structures}"

proc `$`*(mapLocation: MapLocation): string =
  let 
    cells = $mapLocation.cells
    events = $mapLocation.events
    entities = $mapLocation.entities
    structures = $mapLocation.structures
  fmt"<cells: {cells}, terrain: {events} flags: \n\t {entities} \n\t {structures}>"

proc `$`*(mapCell: MapCell): string =
  let
    id = $mapCell.id
    terrain = $mapCell.terrain
    flags = $mapCell.flags
  fmt"<id: {id}, terrain: {terrain} flags: \n\t {flags}>"

proc `$`*(map: Map): string =
  fmt"{map.title}: ({map.width} X {map.height})\n cells: {$map.cells}\n locations: {$map.locations}>"

proc `==`*(cell1, cell2: MapCellEvent): bool =
  cell1.eventCode == cell2.eventCode

export components, readers
