## ECS Component DSL
## Use to easily define components

import ecs
import ast_pattern_matching
import macros, strutils, sequtils, tables, sugar, options, sets

#dumpTree:
#  MagicWeapon(Weapon):
#    damage_type @ "magic"
#    magic_damage: +damage

const DAMAGE_MOD = "damageMod"
const BASE_COMPONENT_NAME = "Component"

# Target build
#dumpTree:
#  type
#    DamageType = enum
#      dtBlunt = "blunt",
#      dtMagic = "magic"
#
#    PositionComponent* = ref object of Component
#      x*: int
#      y*: int
#
#    HealthComponent = ref object of Component
#      value: int
#
#    Weapon = ref object of Component
#      attack: int
#
#    Hammer = ref object of Weapon
#      damageType: DamageType
#
#    MagicWeapon = ref object of Weapon
#      damageType: DamageType
#      magicDamage: int

type
  Component = ref object of RootObj
    ## An ECS base component.
    id: int

  DeferredEnum = object
    ## Enums of new types must be built after all types have been figured out
    section: NimNode
    kinds: seq[NimNode]

  DeferredEnumList = seq[DeferredEnum]
  PragmaDef = tuple[name: string, pragmaDef: NimNode]
  BasePropertyDefs = HashSet[string]

var
  componentIdPool: int = 0

# pass this around
export componentIdPool


proc formatEnumComponentName(ident: NimNode,
                             capCase: bool = false,
                             addPrefix: string = ""): NimNode =
  ## Convert a Type name into a Capitalized name
  ## Adding a prefix will add an enum-style prefix to the name
  ## ie damage_type becomes DamageType and dtDamageType with a prefix

  let val = ident.strVal

  if not val.contains("_") and addPrefix.len == 0:
    return val.ident

  let valSplit = val.split("_")

  var prefix = ""

  if addPrefix.len > 0:
    # Build an enum prefix from the first one/two words
    let prefixSplit = addPrefix.split("_")
    prefix =
      if prefixSplit.len > 1:
        prefixSplit[0][0] & prefixSplit[1][0]
      else:
        prefixSplit[0][0] & prefixSplit[0][1]

  var
    newVal: string

  if valSplit.len == 1:
    newVal = valSplit.join()
  else:
    # If allCaps, we capitalise both, otherwise we camel case it
    newVal =
      if capCase:
        map(valSplit, capitalizeAscii).join()
      else:
        valSplit[0] & valSplit[1].capitalizeAscii()

  # If we have a prefix, insert it at the front
  if prefix.len > 0:
    newVal = prefix & newVal.capitalizeAscii()

  return newVal.ident


proc buildPropDef(prop: NimNode, pragmasToCreate: var seq[PragmaDef],
                  enums: var DeferredEnumList,
                  baseProps = none(BasePropertyDefs)): NimNode =
  ## Build an IdentDefs block to create new component types
  ## Returns a TypeDef

  prop.expectKind nnkStmtList
  result = newNimNode(nnkRecList)
  for child in prop.children:
    child.matchAst:
    of nnkCall(`name` @ nnkIdent, `propType` @ nnkStmtList):
      # Build a name: proptype def
      let propName = formatEnumComponentName(name)

      # Create a identDefs to look like `name`: `NameType`
      propType.matchAst:
      of nnkStmtList(nnkPrefix(
        `prefix` @ nnkIdent,
        `prefName` @ nnkIdent
        )
      ):
        # This is a definition with a modifier prefix (*, +, etc)

        let typ = formatEnumComponentName(propName)
        typ.expectKind nnkIdent

        # Check if the prefix is in one of our defined modifier types
        let modType: string = case $prefix
          of "+":
            DAMAGE_MOD
          else:
            "none"

        # Default to INT
        # TODO: map mod types to int, enum probs?
        let modTypeDef = newIdentNode("int")

        var prag: PragmaDef = (modType, modTypeDef)
        # We need to create these pragmas that we add, so we store the
        # name and type to create
        pragmasToCreate.add(prag)

        result.add(
          newIdentDefs(
            typ.strVal.ident,
            modTypeDef,
            newEmptyNode()
          )
        )
      of nnkStmtList(`propVal` @ nnkIdent):
        result.add(
          newIdentDefs(
            propName.strVal.toLowerAscii().ident,
            propVal.strVal.ident,
            newEmptyNode()
          )
        )
      else:
        discard
    of nnkInfix(ident"@", `propName` @ nnkIdent, `propVal` @ nnkStrLit):
      # This matches a def with a custom Type

      # Check if this is already defined on a parent
      let valName = formatEnumComponentName(propName)
      if isSome(baseProps):
        if valName.strVal in baseProps.get():
          continue

      let
        enumType = formatEnumComponentName(propName, capCase = true)
        enumValName = formatEnumComponentName(propVal,
            addPrefix = propName.strVal)


      enumType.expectKind nnkIdent
      enumValName.expectKind nnkIdent
      valName.expectKind nnkIdent

      let foundEnums = enums.filterIt(it.section == enumType)
      if foundEnums.len > 0:
        let idx = enums.find(foundEnums[0])
        if not (enumValName.strVal in map(enums[idx].kinds, x => x.strVal)):
          enums[idx].kinds.add(enumValName)
      else:
        var defEnum = DeferredEnum(
          section: enumType,
          kinds: @[enumValName]
        )

        enums.add(defEnum)

      result.add(
        newIdentDefs(
          valName,
          enumType,
          newEmptyNode()
        )
      )
    else:
      discard


proc buildInheritedComponentTypeDef(name, base, props: NimNode,
                                    enums: var DeferredEnumList,
                                    baseProps: BasePropertyDefs): NimNode =
  ## Builds a component with inheritance
  ## Inheritance works in the standard sense, where it will inherit all properties of a defined component

  let componentName = name.strVal & BASE_COMPONENT_NAME ## \
    ## The type name has a "Component" suffix

  let baseName = base.strVal & BASE_COMPONENT_NAME
  var pragmas: seq[PragmaDef] = @[]

  let propDefList: NimNode = buildPropDef(props, pragmas, enums, some(baseProps))

  result = newNimNode(nnkTypeDef).add(
    newNimNode(nnkPostfix).add(
      "*".ident,
      componentName.ident
    ),
    newEmptyNode(),
    newNimNode(nnkRefTy).add(
      newNimNode(nnkObjectTy).add(
        newEmptyNode(),
        newNimNode(nnkOfInherit).add(
          baseName.ident
        ),
        propDefList
      )
    )
  )


proc buildComponentTypeDef(name: NimNode,
                           props: NimNode,
                           enums: var DeferredEnumList): NimNode =
  ## Build an individual type entry for a component
  ## Returns a TypeSection

  let componentName = name.strVal & BASE_COMPONENT_NAME ## \
    ## The type name has a "Component" suffix
  var pragmas: seq[PragmaDef] = @[]

  let propDefList: NimNode = buildPropDef(props, pragmas, enums)

  result = newNimNode(nnkTypeDef).add(
    newNimNode(nnkPostfix).add(
      "*".ident,
      componentName.ident
    ),
    newEmptyNode(),
    newNimNode(nnkRefTy).add(
      newNimNode(nnkObjectTy).add(
        newEmptyNode(),
        newNimNode(nnkOfInherit).add(
          BASE_COMPONENT_NAME.ident
      ),
      propDefList
    )
    )
  )


proc enumKindsToNimNodes(enumKinds: seq[NimNode]): NimNode =
  result = newNimNode(nnkEnumTy).add(newEmptyNode())

  # For each enum kind, create an EnumFieldDef
  for enumKind in enumKinds:
    enumKind.expectKind nnkIdent
    result.add(newNimNode(nnkEnumFieldDef).add(
      enumKind,
      newStrLitNode(enumKind.strVal[2 .. ^1].toLowerAscii())
    ))


proc buildBasePropertyDefs(componentList: NimNode,
                           baseName: string): BasePropertyDefs =
  ## Build a seq of strings that are properties that have already been defined
  ## further up the inheritance tree
  componentList.expectKind nnkTypeSection
  let componentBaseName = baseName & BASE_COMPONENT_NAME

  result = initHashset[string]()
  for child in componentList.children:
    child.matchAst:
    of nnkTypeDef(nnkPostfix(
      _, `base` @ nnkIdent
      ), _, nnkRefTy(nnkObjectTy(
        _, `parentBaseType` @ nnkOfInherit, `props` @ nnkRecList)
      )
    ):
      if base.strVal == componentBaseName:
        for propChild in props.children:
          propChild.matchAst:
          of nnkIdentDefs(`name` @ nnkIdent, _, _):
            result.incl(name.strVal)
          else:
            echo "Missed " & props.treeRepr
        if parentBaseType[0].strVal != BASE_COMPONENT_NAME:
          result = result + buildBasePropertyDefs(componentList, parentBaseType[0].strVal)


macro components*(body: untyped): untyped =
  ## Given a list of components, convert this into a nnkTypeSection
  ## And convert each definition to a TypeDef

  body.expectKind nnkStmtList

  result = newTree(nnkStmtList)
  var
    n = newNimNode(nnkTypeSection)
    deferredEnums: DeferredEnumList = @[]
    baseProperties: TableRef[string, BasePropertyDefs] = newTable[string,
        BasePropertyDefs]()

  # Go through the components and convert them to types
  for child in body.children:
    child.matchAst:
    of nnkCall(
      `name` @ nnkIdent,
      `props` @ nnkStmtList
    ):
      # Regular component
      # Get deferred enums and
      n.add(buildComponentTypeDef(name, props, deferredEnums))
    of nnkCall(
      `name` @ nnkIdent,
      `base` @ nnkIdent,
      `props` @ nnkStmtList
    ):
      # Component with inheritance
      # Save a table of derived enum properties to make sure we don't double up on any
      var baseProps: BasePropertyDefs = initHashSet[string]()
      if baseProperties.contains(base.strVal):
        baseProps = baseProperties[base.strVal]
      else:
        baseProps = n.buildBasePropertyDefs(base.strVal)
        baseProperties[base.strVal] = baseProps

      n.add(buildInheritedComponentTypeDef(name, base, props, deferredEnums, baseProps))
    else:
      # Dunno? Abort maybe?
      discard

  # For each deferred enum, create an Enum Type
  for defEnum in deferredEnums:
    let enumDef = newNimNode(nnkTypeDef).add(
      defEnum.section,
      newEmptyNode(),
      enumKindsToNimNodes(defEnum.kinds)
    )
    n.insert(
      0,
      enumDef
    )
  result.add(n)


when isMainModule:
  # Sample input
  components:
    Position:
      x: int
      y: int
    Health:
      value: int
    Weapon:
      attack: int
    Hammer(Weapon):
      damage_type @ "blunt"
    MagicWeapon(Weapon):
      damage_type @ "magic"
      magic_damage: +damage
    Bow(Weapon):
      damage_type @ "piercing"
      weapon_range @ "short"
    IceBow(Bow):
      weapon_range @ "long"
    LongBow(Bow):
      weapon_range @ "far"


  let h = HealthComponent(value: 5)
  let mw = MagicWeaponComponent(attack: 5, damageType: dtMagic)
  let w = newWorld()
  let e1 = w.newEntity()

  e1.addComponent(h)

  w.prepareForProcessing()

  var
    e1x = e1.getComponentPtr(HealthComponent).value
  echo "Done!"
