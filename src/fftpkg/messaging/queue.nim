import os

when defined(TEST):
  type
    System* {.pure, size: sizeof(byte).} = enum 
      AU = "Audio",
      RE = "Renderer",
      UT = "Utilities"
      KB = "Keyboard",
      MO = "Mouse",
      DI = "Display"
      TEST = "Test module"
      MD = "Message Dispatch"
else:
  type
    System* {.pure, size: sizeof(byte).} = enum 
      AU = "Audio",
      RE = "Renderer",
      UT = "Utilities"
      KB = "Keyboard",
      MO = "Mouse",
      DI = "Display"
      MD = "Message Dispatch"

type
  Message* {.packed, bycopy.} = object
    `to`: System
    `from`: System
    data: string
    `type`: string

proc `$`*(m: Message): string =
  $m.to & ":" & $m.from & "::" & m.data & "::" & m.type

when defined(TEST):
  var testChan*: Channel[Message]
  testChan.open()
  proc listen() = 
    echo "Thread is starting"
    while true:
      let tried = testChan.tryRecv()
      if tried.dataAvailable:
        echo "Received data from HQ"
        echo tried.msg

        var pong = Message()
        pong.to = System.TEST
        pong.from = System.MD
        pong.data = "TEST RECEIVED"
        pong.type = "string"
        testChan.send(pong)
      sleep(400)

  var messenger1: Thread[void]
  createThread(messenger1, listen)

proc sendMessage*() =
  var msg = Message()
  msg.to = System.AU
  msg.from = System.KB
  msg.data = "0000"
  msg.type = "string"
  when defined(TEST):
    testChan.send(msg)


when isMainModule:
  var worker1: Thread[void]

  createThread(worker1, sendMessage)
