import random
import "../lib"

randomize()

type

  Factory*[T: SomeMapType] = ref FactoryObj[T]
  FactoryObj[T: SomeMapType] = object

  ObjectFactory*[T: SomeObjectType] = ref ObjectFactoryObj[T]
  ObjectFactoryObj[T: SomeObjectType] = object

  SomeObjectType* = SomeMapEvent or SomeMapEntity or SomeMapStructure
  FactoryTypes* = SomeMapType or SomeObjectType
  EventObjectTypes* = MapLocation or MapCellFlags


proc populate*(event: var SomeMapEvent) =
  event.eventCode = rand(256).cuint


proc populate*(entity: var SomeMapEntity) =
  entity.isHostile = rand(1).cuint
  entity.entityCode = rand(127).cuint
  

proc populate*(structure: var SomeMapStructure) =
  structure.isHostile = rand(1).cuint
  structure.structureCode = rand(127).cuint


proc create*(factory: ObjectFactory, random: bool = false): ObjectFactory.T =
  if random:
    result.populate()


proc populate*(mapobj: var EventObjectTypes, random: bool = false) =
  when mapobj is MapCellFlags: 
    let
      eventSize = MAP_CELL_MAX_EVENTS
      entitySize = MAP_CELL_MAX_ENTITIES
      structSize = MAP_CELL_MAX_STRUCTURES

    var 
      eventFac = new ObjectFactory[MapCellEvent]
      entityFac = new ObjectFactory[MapCellEntity]
      structFac = new ObjectFactory[MapCellStructure]

  when mapobj is MapLocation:
    let 
      eventSize = MAP_LOCATION_MAX_EVENTS
      entitySize = MAP_LOCATION_MAX_ENTITIES
      structSize = MAP_LOCATION_MAX_STRUCTURES

    var 
      eventFac = new ObjectFactory[MapLocationEvent]
      entityFac = new ObjectFactory[MapLocationEntity]
      structFac = new ObjectFactory[MapLocationStructure]

    for i in 0 ..< MAP_LOCATION_MAX_CELL_SIZE:
      mapobj.cells[i] = rand(9999).int32

  for i in 0 ..< eventSize:
    mapobj.events[i] = eventFac.create(random = random)
  for i in 0 ..< entitySize:
    mapobj.entities[i] = entityFac.create(random = random)
  for i in 0 ..< structSize:
    mapobj.structures[i] = structFac.create(random = random)
    

proc populate*(cell: var MapCell, random: bool = false) =
  cell.id = rand(9999).int32
  let ttype = TerrainType(TerrainType.high.ord.rand())
  cell.terrain = ttype
  if random:
    var flags: MapCellFlags
    flags.populate(random = random)


proc create*[T](factory: Factory[T], random: bool = false): T =
  new result

  var r: T = new T
  if random:
    populate(r, random = true)
  return r


proc newObjectFactory*[T](): ObjectFactory[T] =
  new result

proc newFactory*[T](): Factory[T] =
  new result

when isMainModule:
  var f = newFactory[MapCell]()
    
  for i in 0 .. 20:
    let cc = f.create(random = true)
    echo cc.terrain

  var f2 = new Factory[MapLocation]
  var b2 = f2.create(random = true)
  assert b2 is MapLocation

  var f3 = newFactory[MapCell]()
  var f4 = newObjectFactory[MapCellEvent]()
