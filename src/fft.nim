import csfml
from fftpkg/window import initWindow


proc main_loop() =
  let window = initWindow()

  while window.open:
    var event: Event
    while window.pollEvent(event):
      if event.kind == EventType.Closed or (event.kind == EventType.KeyPressed and event.key.code == KeyCode.Escape):
        window.close()
        break

    window.clear color(0, 0, 0)
    window.display()
  window.destroy()

when isMainModule:
  main_loop()
