# Package

version       = "0.1.0"
author        = "ryancotter"
description   = "Fight For Tangaria - A game inspired by Ogre Battle"
license       = "MIT"
srcDir        = "src"
bin           = @["fft"]

# Dependencies

requires "nim >= 0.20.9"
requires "csfml#head"
requires "parsetoml#head"
requires "iterutils"
requires "zip"
requires "https://github.com/yglukhov/ecs"
requires "ast_pattern_matching"
requires "coverage"
