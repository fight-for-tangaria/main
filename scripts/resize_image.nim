import os, re, strformat
import nimagemagick

proc main =
  for kind, file in walkDir joinPath(parentDir(getCurrentDir()), "assets"):
    if kind == pcFile and file.match re".*\.png":
      echo file
      var wand = newWand(file)
      if wand.width != 64 and wand.height != 64:
        #Back up the old file
        copyFile(file, fmt"{file}._old")
        discard wand.resizeImage(64, 64)
        wand.writeImage(file)

when isMainModule:
  genesis()

  main()

  terminus()
