#!/usr/bin/zsh
autoload zmv

cd assets
file *.png | grep -v 64 | cut -d: -f1 | xargs -I {} convert {} -resize 64x64 {}_new
file *.png | grep -v 64 | cut -d: -f1 | xargs -I {} mv {} {}_old
zmv '(*)-*.png_new' '$1.png'
