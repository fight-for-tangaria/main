# Overview

Very much a proof of concept, very WIP

# Building Stuff

## Building Cell Files (.mfc)
Each element is delimited by a `;` character. Each line is separated by a `\n` character
Format: 
  * id: int32
  * events: Up to 100 uint8
  * entities: Up to 100 uint8
  * structures: Up to 10 uint8

## Building Cell Location files (.mf)
Each element is delimited by a `;` character. Each line is separated by a `\n` character
Format:
  * cell ids: Up to 64 32int cell ids
  * events: Up to 10 uint8
  * entities: Up to 50 uint8
  * structures: Up to 8 uint8

## Building Terrain Files (.tf)
Types:
  * Plains = "=",
  * Forest = "#",
  * Mountain = "^",
  * River = "~",
  * Lake = "*",
  * Ocean = "@"

