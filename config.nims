let srcPath = thisDir() & "/src/fftpkg/"

switch("path", srcPath)
switch("threads", "on")
switch("verbosity", "2")

task render_tests, "Run Render Tests":
  exec "nim c -r " & srcPath & "renderer.nim"

task resize_assets, "Resize assets to 64x64":
  exec "cd scripts; nim c -r resize_image.nim; cd .."

task rct, "Run Component tests":
  exec "nim c  -r " & srcPath & "messaging/components.nim"

task testMessages, "Run Tests":
  exec "nim c -r -d:TEST tests/test_messaging.nim"
